# Culture Jam
[![forthebadge](https://forthebadge.com/images/badges/made-with-javascript.svg)](https://forthebadge.com)

Par Jade LLANES et Maximilien COSTA

![](https://gitlab.com/MCYnov/culture-jam/-/wikis/uploads/a4e39d138cf4eef121168114dff5165a/culturejam-pitit__2_.png)

#### Description
Culture Jam est une PWA qui permet de consulter les différentes caractéristiques culturelles d’un pays.

[Lien vers le wiki](https://gitlab.com/MCYnov/culture-jam/-/wikis/Pr%C3%A9sentation-du-projet)

[Lien vers le git du backend](https://github.com/MCYnov/Culture-Jam-Back)
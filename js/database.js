/**
 * @name database.js
 * @project Culture Jam
 * @version 1.0.0
 * @author Maximilien COSTA & Jade LLANES
 */


/**
 * Récupération des données
 * @param {string} type Page (carte ou couleur)
 * @param {string} key Pays ou couleur
 */
function displayKey(type, key){

    onlineTest(function(){
        let link = "https://culture-jam.herokuapp.com/";
        
        if(type === "carte"){
            link += "article/" + key;
        }else{
            link += "color/" + key;
        }

        $.ajax({
            method: "GET",
            url: link,
            timeout: 10000,
            success: function(res){
                if(res == "0"){
                    displayOfflineMode(type, key); 
                }else{
                    saveData(key, res);
                    displayOnlineMode(type, res);
                }
            },
            error: function(err){
                console.error("Erreur get " + type + " ! \n " + err);
                displayOfflineMode(type, key);
            }
        });
    },
    function(){
        displayOfflineMode(type, key);
    })
}



// Fonction de test de connection avec l'api de culture jam. Cette fonction permet de déterminer si l'application en mode ligne ou hors ligne 
function onlineTest(online, offline){
    $.ajax({
        method: "GET",
        url: "https://culture-jam.herokuapp.com/ping",
        timeout: 3000,
        success: function(res){
            if(res === "pong"){
                online();
            }else{
                offline();
            }
        },
        error: function(err){
            console.error("Erreur : ", err);
            offline();
        }
    });
}

// Fonction de sauvegarde dans le LocalStorage
function saveData(key, data){
    if (typeof localStorage != 'undefined') {
        localStorage.setItem(key, data.content);
    }else{
        console.error("Erreur de sauvegarde !");
    }
}

// Affichage en mode en ligne
function displayOnlineMode(type, data){
    let div = (type === "carte") ? document.getElementById("article") : document.getElementById("color");
    let html = null;

    div.innerHTML = "";
    html = new GeneHTML(JSON.parse(data.content), 0, div, {
        boot: true
    });
}

// Affichage en mode hors ligne
function displayOfflineMode(type, key){
    let div = (type === "carte") ? document.getElementById("article") : document.getElementById("color");
    let html = null;

    if (typeof localStorage != 'undefined') {
        if (key in localStorage) {
            div.innerHTML = "";
            html = new GeneHTML(JSON.parse(localStorage.getItem(key)), 0, div, {
                boot: true
            });
        }else{
            let text = (type === "carte") ? "La culture de ce pays n'a pas encore été publié par Culture Jam ! Nous vous préviendrons quand nous l'aurons ajouté !" : "Cette couleur n'a pas encore était traité !"
            div.innerHTML = "";
            html = new GeneHTML([{ element: "p", text: "Prochainement ..." }, { element: "p", text: text }], 0, div, {
                boot: true
            });
        }
    } else {
        console.log("sessionStorage n'est pas supporté");
        div.innerHTML = "";
        html = new GeneHTML([{ element: "p", text: "Erreur" }, { element: "p", text: "Une erreur est survenu !" }], 0, div, {
            boot: true
        });
    }
}
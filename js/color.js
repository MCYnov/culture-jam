/**
 * @name Color.js
 * @project Culture Jam
 * @version 1.0.0
 * @author Maximilien COSTA & Jade LLANES
 */

var colorDiv = document.getElementById("color");
var colorHtml = null;

// Initialisation de la palette de couleur 
$(document).ready(function () {
    $("#widget").colorwheel('init', ['0010A5', '6300A5', 'FF0000', 'FF6600', 'FFFF00', '0FAD00', 'FFFFFF','000000']);
})

// Evénement lorsque l'on clique sur une couleur, cela va appeller la fonction displayInfoByColor qui va afficher les articles par rapport à la couleur
document.getElementById("widget").addEventListener("click", function(){
    let color = $(this).colorwheel('value');
    displayInfoByColor(color);
});


/**
 * Affiche l'article en fonction du code couleur qui va ensuite permettre de récupérer toutes les informations de ce dernier
 * @param {string} color Code couleur (ex: #fff)
 */
function displayInfoByColor(color) {
    displayKey("color", color);
}


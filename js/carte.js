/**
 * @name Carte.js
 * @project Culture Jam
 * @version 1.0.0
 * @author Maximilien COSTA & Jade LLANES
 */

// Variable
var articleDiv = document.getElementById("article");
var articleHtml = null;


// Initialisation carte
$(function () {
    $('#world-map').vectorMap({ 
        map: 'world_mill',
        onRegionClick: function (e, code) {
            displayInfoByCountry(code);
        },
        
    });

});

/**
 * Affiche l'article en fonction du code du pays qui va ensuite permettre de récupérer toutes les informations de ce dernier
 * @param {string} code Code du pays (ex: FR pour la France)
 */
function displayInfoByCountry(code) {
    displayKey("carte", code);
}

// Vibration test
function toVibrate(){
    navigator.vibrate(500);
    console.log("Vibration : ok");
}


/**
* GeneHTML
* v1.3
* By Maximilien COSTA
*/


class GeneHTML {

    /**
     * 
     * @param {json} templateJson Template d'une partie en HTML convertie en JSON celon ma synthaxe
     * @param {number} index Index propre au code qui va être générer
     * @param {DOM} baseHtml Element parent qui va recevoir de code HTML générer 
     * @param {objet} option Objet contenant divers option, comme "data" (qui est un tableau qui contient des variables qui vont être insérer dans le code), ou encore boot [true/false](permet d'activer la génération du code html dès la création de GeneHTML ou au contraire de l'empêcher) 
     */
    constructor(templateJson, index, baseHtml, option = {}) {
        this.templateJson = templateJson;
        this.index = index;
        this.baseHtml = baseHtml;
        this.data = null;
        this.element = null;
        this.boot = true;
        this.state = true;
        this.initoption(option);

        this.templateJson.forEach((value) => {
            var element = this.createElement(value.element);
            if (value.attribute) {
                element = this.setAttributeToElement(element, value.attribute);
            }
            if (value.text) {
                element = this.setText(element, value.text);
            }
            if (value.html) {
                element = this.setInnerHtml(element, value.html);
            }
            if (value.event) {
                element = this.setEventToElement(element, value.event);
            }
            if (value.child) {
                element = this.geneChild(element, value.child);
            }
            if (this.boot === true) {
                this.baseHtml.appendChild(element);
            }
            this.element = element;
        });

        // Hide element
        this.hide = () => {
            if (this.state === true) {
                try {
                    this.element.parentElement.removeChild(this.element);
                    this.state = false;
                } catch (e) {
                    console.error("[GENEHTML] [ERROR] [HIDE] : ", e);
                }
            }
        }

        // Show element
        this.show = () => {
            if (this.state === false) {
                try {
                    this.baseHtml.appendChild(this.element);
                    this.state = true;
                } catch (e) {
                    console.error("[GENEHTML] [ERROR] [SHOW] : ", e);
                }
            }
        }

    }


    // <data i> --> remplace par index ; <data 1> --> remplace par une variable dynamique

    /**
     * Remplace <data i> par l'index général
     * @param {string} str Texte a traiter
     * @returns {string} Texte traiter
     */
    splitText(str) {
        if (str) {
            var tab = str.split("<data i>");
            var text = "";

            tab.forEach((value, index) => {
                if (index != (tab.length - 1)) {
                    text += value + this.index;
                } else {
                    text += value;
                }
            });

            text = this.applyBalise(text);
            return text;
        }
    }


    /**
     * Remplace <data x> par l"élément à l'index x du tableau data
     * @param {string} str Text a traiter
     * @returns {string} Text traiter
     */
    applyBalise(str) {

        var _str = "";
        if ((this.data) && (str) && (this.data.length)) {

            this.data.forEach((value, index) => {
                var _text = "";
                if (_str == "") {
                    var tab = str.split("<data " + index + ">");
                } else {
                    var tab = _str.split("<data " + index + ">");
                }
                tab.forEach((value1, index1) => {
                    if (index1 != (tab.length - 1)) {
                        _text += value1 + value;
                    } else {
                        _text += value1;
                    }
                });

                _str = _text;
            });
        } else {
            _str = str;
        }
        return _str;

    }

    setInnerHtml(element, html) {
        if (html) {
            element.innerHTML = this.splitText(html);
            return element;
        }
    }


    createElement(element) {
        return document.createElement(element);
    }

    setAttributeToElement(element, attribute) {
        if (element) {
            if (attribute) {
                attribute.forEach(att => {
                    element.setAttribute(att[0], this.splitText(att[1]));
                });
                return element;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    setText(element, text) {
        // console.log("[GENEHTML] text : ", text);
        element.appendChild(document.createTextNode(this.splitText(text)));
        // console.log("[GENEHTML] text : ", element);
        return element;
    }


    // eval() pour utiliser les data
    setEventToElement(element, event) {
        if (element.attachEvent) {
            //Est-ce IE ?
            element.attachEvent("on" + event.type, event.fonction);
        } else {
            element.addEventListener(event.type, event.fonction, true);
        }

        return element;
    }


    geneChild(element, childs) {

        childs.forEach(child => {
            var _child = this.createElement(child.element);
            if (child.attribute) {
                _child = this.setAttributeToElement(_child, child.attribute);
            }
            if (child.text) {
                _child = this.setText(_child, child.text);
            }
            if (child.html) {
                _child = this.setInnerHtml(_child, child.html);
            }
            if (child.event) {
                _child = this.setEventToElement(_child, child.event);
            }
            if (child.child) {
                _child = this.geneChild(_child, child.child);
            }
            element.appendChild(_child);
        });

        return element;
    }

    initoption(opts) {
        for (var opt in opts) {
            switch (opt) {
                case "boot":
                    this.boot = opts[opt];
                    this.state = this.boot;
                    break;

                case "data":
                    this.data = opts[opt];
                    break;

                default:
                    console.warn("[GENEHTML] Option non supporté ! ("+opt+")");
                    break;
            }
        }
    }

}



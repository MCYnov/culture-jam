const CACHE_NAME = 'culturejam-cache-v1';

const FILES_TO_CACHE = [
    './index.html',
    './color.html',
    './js/lib/jquery-3.4.1.min.js',
    './js/lib/jquery-jvectormap-2.0.5.min.js',
    './js/lib/jquery-jvectormap-world-mill.js',
    './js/lib/geneHtml.js',
    './css/w3.css',
    './css/style.css',
    './css/jquery-jvectormap-2.0.5.css',
    './css/jquery.colorwheel.css',
    './js/lib/jquery.colorwheel.js',
    './js/color.js',
    './js/carte.js',
    './js/database.js'
];

self.addEventListener('install', (e) => {
    console.log('👷', 'install', e);
    e.waitUntil(
        caches.open(CACHE_NAME).then((cache) => {
            console.log('[Service Worker] Mise en cache globale: app shell et contenu');
            return cache.addAll(FILES_TO_CACHE);
        }, (err) => {
            console.log("ERROR : " + err);
        })
    );
});

self.addEventListener('activate', (e) => {
    e.waitUntil(
        caches.keys().then((keyList) => {
            return Promise.all(keyList.map((key) => {
                if (CACHE_NAME.indexOf(key) === -1) {
                    return caches.delete(key);
                }
            }));
        })
    );
});

self.addEventListener('fetch', (evt) => {
    evt.respondWith(
        caches.open(CACHE_NAME).then((cache) => {
            return cache.match(evt.request)
                .then((response) => {
                    return response || fetch(evt.request);
                });
        })
    );
});